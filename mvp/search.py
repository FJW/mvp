from typing import Pattern, Generator

import os
import sys
from pathlib import Path
from collections import defaultdict

from mvp.config import Settings, Root, best_root


def file_match(file: Path, patterns: list[Pattern[str]]) -> bool:
    filename = file.as_posix()
    for pattern in patterns:
        if pattern.search(filename) is None:
            return False
    return True


def collect_files(
    patterns: list[Pattern[str]], settings: Settings
) -> Generator[tuple[Path, Root], None, None]:
    for root in settings.root_directories:
        try:
            for file in root.path.glob("**/*"):
                if (
                    os.path.isfile(file)
                    and file.suffix in settings.suffixes
                    and file_match(file, patterns)
                ):
                    yield file.relative_to(root.path), root
        except OSError as e:
            sys.stderr.write(
                f"Error when trying to traverse {root.name} ({root.path}): {str(e)}\n"
            )
            pass


def search(patterns: list[Pattern[str]], settings: Settings) -> list[tuple[Path, Root]]:
    results: dict[Path, list[Root]] = defaultdict(lambda: list())
    for file, root in collect_files(patterns, settings):
        results[file].append(root)
    return [(file, best_root(roots)) for file, roots in results.items()]
