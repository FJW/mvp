import subprocess

from pathlib import Path

from mvp.config import Settings, Root


def play(path: Path, root: Root, settings: Settings) -> int:
    source_path = root.path.joinpath(path)
    return subprocess.run(
        [settings.video_player] + settings.player_args + [source_path.as_posix()]
    ).returncode
