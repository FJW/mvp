from dataclasses import dataclass, field

from pathlib import Path


@dataclass
class Root:
    path: Path
    name: str
    priority: int


def best_root(candidates: list[Root]) -> Root:
    assert len(candidates) > 0
    return min(candidates, key=lambda r: r.priority)


@dataclass
class Settings:
    video_player: str
    root_directories: list[Root]
    max_results: int = 200
    player_args: list[str] = field(default_factory=list)
    suffixes: list[str] = field(
        default_factory=lambda: [".mkv", ".mp4", ".avi", ".webm", ".iso"]
    )
