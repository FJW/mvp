#! /usr/bin/python3

import sys
import re
from pathlib import Path

from mvp.config import Settings, Root
import mvp.play as play
import mvp.search as search


def print_results(results: list[tuple[Path, Root]]) -> None:
    digits = len(str(len(results)))
    for i, (path, root) in enumerate(results):
        print(f"{i+1:{digits}}: {path}  ({root.name})")


def parse_index(string: str, n: int) -> int:
    if string == "":
        return 0
    ret = int(string) - 1
    if ret < 0 or ret >= n:
        raise Exception("Index out of bounds")
    return ret


def select_result(results: list[tuple[Path, Root]], settings: Settings) -> int:
    num_results = len(results)
    if num_results == 0:
        raise Exception("No Matches")
    elif num_results == 1:
        print("Found one match")
        return 0
    elif num_results > settings.max_results:
        raise Exception("Too many results")
    else:
        print("Found multiple results, enter index to select (default = 1):")
        print_results(results)
        return parse_index(input(">>> "), num_results)


def main(args: list[str]) -> int:
    settings = Settings("/usr/bin/mpv", [Root(Path("/home/mm/videos/"), "mm", 0)])
    patterns = [re.compile(arg, re.I) for arg in args[1:]]
    search_results = sorted(search.search(patterns, settings))
    index = select_result(search_results, settings)
    path, root = search_results[index]
    return play.play(path, root, settings)


if __name__ == "__main__":
    try:
        sys.exit(main(sys.argv))
    except BaseException as e:
        print("Error: ", str(e))
        sys.exit(-1)
