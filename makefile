SOURCE_DIR="mvp"

.PHONY: all typecheck format

all: format typecheck

typecheck:
	mypy --strict $(SOURCE_DIR)

format:
	black $(SOURCE_DIR)
