My Video Player
===============


Usage
-----

`mvp [some] [search] [patterns]`

Will search for files that contain a match for each of the provided regexes; If there is exactly one match it will play that file, otherwise list the available options and ask for a selection.